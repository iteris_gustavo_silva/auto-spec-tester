package br.com.iteris.data.spec.tester.model.repository;

import br.com.iteris.data.jpa.spec.auto.repository.FilterCriteriaRepository;
import br.com.iteris.data.spec.tester.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long>, JpaSpecificationExecutor<Person>, FilterCriteriaRepository<Person> {
}
