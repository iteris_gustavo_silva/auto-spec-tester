package br.com.iteris.data.spec.tester.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class PersonDTO {
    private String name;
    private String firstName;
}
