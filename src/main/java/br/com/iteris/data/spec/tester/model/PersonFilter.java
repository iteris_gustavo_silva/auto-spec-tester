package br.com.iteris.data.spec.tester.model;

import br.com.iteris.data.jpa.spec.auto.filter.EntityFilter;
import br.com.iteris.data.jpa.spec.auto.internal.Operation;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecAlternative;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecProperty;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecRange;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonFilter implements EntityFilter<Person> {

    @SpecProperty
    private String name;

    @SpecProperty
    private List<String> nameIn;

    @SpecProperty
    @SpecAlternative({"firstName", "lastName"})
    private String nameLike;

    @SpecProperty(value = "name", operation = Operation.STARTS_WITH)
    private String nameStart;

    @SpecProperty
    private String nameEndsWith;

    @SpecProperty(value = "address_city", operation = Operation.LIKE)
    private String city;

    @SpecProperty("address_country")
    private String country;

    @SpecProperty("address_number")
    private Integer number;

    @SpecRange("birthDate")
    private LocalDate startBirth;

    @SpecRange("birthDate")
    private LocalDate endBirth;

}
