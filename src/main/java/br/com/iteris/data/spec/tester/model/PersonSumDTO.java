package br.com.iteris.data.spec.tester.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class PersonSumDTO {
    private long count;
    private String name;
}
