package br.com.iteris.data.spec.tester;

import br.com.iteris.data.jpa.spec.auto.repository.FilterCriteriaRepositoryImpl;
import br.com.iteris.data.spec.tester.model.*;
import br.com.iteris.data.spec.tester.model.repository.PersonRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;

@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = FilterCriteriaRepositoryImpl.class)
public class Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(Application.class, args);
        PersonRepository bean = run.getBean(PersonRepository.class);

//        PersonFilter filter = PersonFilter.builder()
//                .nameStart("Gustavo")
//                .build();

        PersonDTOFilter filter = PersonDTOFilter.builder()
//                .city("Brasília")
                .build();
        List<PersonDTO> all = bean.findAll(filter);
//        Long count = bean.count(filter.toSpecification());
        System.out.println("\n\n\n\n");

        PersonSumDTOFilter filter2 = PersonSumDTOFilter.builder()
//                .city("Brasília")
                .build();
        List<PersonSumDTO> all2 = bean.findAll(filter2);
//        Long count = bean.count(filter.toSpecification());
        all.forEach(System.out::println);
        System.out.println("\n\n\n\n");

//        System.out.println("Found " + count + " records.");
        all2.forEach(System.out::println);
        System.out.println("\n\n\n\n");
    }
}
