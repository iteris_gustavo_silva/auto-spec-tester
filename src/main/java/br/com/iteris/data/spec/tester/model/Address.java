package br.com.iteris.data.spec.tester.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
@NoArgsConstructor
public class Address {

    private String streetName;

    @Column(name = "address_number")
    private Integer number;

    private String neighborhood;
    private String city;
    private String province;
    private String country;
}
