package br.com.iteris.data.spec.tester.model;

import br.com.iteris.data.jpa.spec.auto.filter.TransferFilter;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecAlternative;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecProperty;
import lombok.Builder;
import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Data
@Builder
public class PersonSumDTOFilter implements TransferFilter<Person, PersonSumDTO> {

    @SpecProperty
    @SpecAlternative({"firstName", "lastName"})
    private String name;

    @SpecProperty("address_city")
    private String city;

    @Override
    public CriteriaQuery<PersonSumDTO> defineSelection(Root<Person> root, CriteriaQuery<PersonSumDTO> query, CriteriaBuilder builder) {
        return query.select(builder.construct(PersonSumDTO.class, builder.sum(root.get("age")), root.get("name")))
                .groupBy(root.get("name"));
    }
}
