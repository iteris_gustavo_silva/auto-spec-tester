package br.com.iteris.data.spec.tester.model;

import lombok.*;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
public class Person {

    @Id
    private Long id;

    private String name;
    private String firstName;
    private String lastName;
    private Long age;
    private LocalDate birthDate;
    private LocalDateTime createdAt;

    @Embedded
    private Address address;

    public Person(String name) {
        this.name = name;
    }
}
