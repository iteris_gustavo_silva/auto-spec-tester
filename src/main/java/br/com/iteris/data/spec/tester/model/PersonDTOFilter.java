package br.com.iteris.data.spec.tester.model;

import br.com.iteris.data.jpa.spec.auto.filter.TransferFilter;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecAlternative;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecProperty;
import lombok.Builder;
import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Data
@Builder
public class PersonDTOFilter implements TransferFilter<Person, PersonDTO> {

    @SpecProperty
    @SpecAlternative({"firstName", "lastName"})
    private String name;

    @SpecProperty("address_city")
    private String city;

    @Override
    public CriteriaQuery<PersonDTO> defineSelection(Root<Person> root, CriteriaQuery<PersonDTO> query, CriteriaBuilder builder) {
        return query.select(builder.construct(PersonDTO.class, root.get("name"), root.get("firstName")));
    }
}
